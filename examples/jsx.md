```tsx

h('div',{},
  h('h1',null,'Hello HyperText')
  h(MyComp,{ params:123 })
)

interface User {
  id: string;
  name: string;
  color: string;
  pet: {
    name: string;
  };
}

const users: User[] = [
  {
    id: "234",
    name: "Alice",
    color: "red",
    pet: {
      name: "a cat",
    },
  },
  {
    id: "123",
    name: "Bob",
    color: "green",
    pet: {
      name: "a dog",
    },
  },
  {
    id: "345",
    name: "Kate",
    color: "blue ",
    pet: {
      name: "a bird",
    },
  },
];

interface Props {
  user: User;
  children: React.ReactNode;
}
const user = users[0];

const UserItem = (props: Props) => {
  const user = props.user;
  return (
    <div
      className="list-group-item"
      id={"user_" + user.id}
      style={{ color: user.color }}
    >
      {/* comment */}
      <span>
        {user.name} has {user.pet.name}
      </span>
      {props.children ? props.children : null}
    </div>
  );
};
      // {props.children ? <UserItem/> : null}

const UserItemsList = () => (
  <div className="list-group m-3">
    {users.map((user) => {
      // return <div></div>
      // return UserItem({ user: user, children: <button>Like</button> });
      return (
        // { UserItem({ user: user, children: <button>Like</button> })}
        <UserItem user={user} key={user.id}>
          <button>Like</button>
        </UserItem>
      );
    })}
  </div>
);

ReactDOM.render(<UserItemsList />, document.getElementById("root"));

```