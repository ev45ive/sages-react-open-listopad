```js

[1,2,3,4,5].reduce( (sum,x) => {
    console.log(sum, x, sum + x) 
    return sum + x 
},0)

[1,2,3,4,5].reduce( (state,x) => {
//     console.log(state, x, state.counter + x) 
    return {
        ...state,
        counter: state.counter + x 
    } 
},{
    counter: 0, todos:[] 
})


inc = (payload=1) => ({type:'INC', payload});
dec = (payload=1) => ({type:'DEC', payload});
addTodo = (payload) => ({type:'ADDTODO', payload});

[inc(),inc(2),addTodo('kup mleko'),dec(1)].reduce( (state,action) => {

    switch(action.type){
        case 'INC': return {...state, counter:state.counter + action.payload};
        case 'DEC': return {...state, counter:state.counter - action.payload};
        case 'ADDTODO': return {...state, todos: [...state.todos, action.payload] };
        default: return state
    }    
},{
    counter: 0, todos:[] 
})
// {counter: 2, todos: Array(1)}
    // counter: 2
    // todos: ['kup mleko']


inc = (payload=1) => ({type:'INC', payload});
dec = (payload=1) => ({type:'DEC', payload});
addTodo = (payload) => ({type:'ADDTODO', payload});

reducer = (state,action) => {
    switch(action.type){
        case 'INC': return {...state, counter:state.counter + action.payload};
        case 'DEC': return {...state, counter:state.counter - action.payload};
        case 'ADDTODO': return {...state, todos: [...state.todos, action.payload] };
        default: return state
    }    
}

initialState = {
    counter: 0, todos:[] 
}

dispatch(action){
    state = reducer(state,action)
    console.log(state);
}

dispatch( inc() )
dispatch( inc(2) )
dispatch( addTodo('kup mleko') )
dispatch( dec(1) )

```


```ts

inc = (payload=1) => ({type:'INC', payload});
dec = (payload=1) => ({type:'DEC', payload});
addTodo = (payload) => ({type:'ADDTODO', payload});

counterReducer = (state = 0, action) => {
    switch(action.type){
        case 'INC': return  state + action.payload;
        case 'DEC': return state- action.payload;
        default: return state
    }
}

reducer = (state,action) => {
    switch(action.type){
//         case 'INC': return {...state, counter:state.counter + action.payload};
//         case 'DEC': return {...state, counter:state.counter - action.payload};
        case 'ADDTODO': return {...state, todos: [...state.todos, action.payload] };
        default: return { 
            ...state, 
            counter: counterReducer(state.counter,action)
        }
    }    
}

initialState = {
    counter: 0, todos:[] 
}
state = initialState

dispatch = (action) => {
    state = reducer(state,action)
    console.log(state);
}

dispatch( inc() )
dispatch( inc(2) )
dispatch( addTodo('kup mleko') )
dispatch( dec(1) )


// VM5020:31 {counter: 1, todos: Array(0)}
// VM5020:31 {counter: 3, todos: Array(0)}
// VM5020:31 {counter: 3, todos: Array(1)}
// VM5020:31 {counter: 2, todos: Array(1)}

```