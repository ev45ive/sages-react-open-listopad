```ts


interface User {
  id: string;
  name: string;
  color: string;
  pet: {
    name: string;
  };
}

const users: User[] = [
  {
    id: "234",
    name: "Alice",
    color: "red",
    pet: {
      name: "a cat",
    },
  },
  {
    id: "123",
    name: "Bob",
    color: "green",
    pet: {
      name: "a dog",
    },
  },
  {
    id: "345",
    name: "Kate",
    color: "blue ",
    pet: {
      name: "a bird",
    },
  },
];

interface Props {
  user: User;
  children: React.ReactNode;
}
const user = users[0];

const UserSection = (props: Props) => {
  const user = props.user;

  return React.createElement(
    "div",
    {
      className: "list-group-item",
      style: { color: user.color },
    },
    React.createElement("span", {}, user.name + " has "),
    React.createElement("span", {}, user.pet.name),
    props.children
  );
};

const UserList = React.createElement(
  "div",
  { className: "list-group m-3" },
  users.map((user) => {
    // return UserSection({ user: user });
    // React Function Component
    return React.createElement(UserItem, {
      user: user,
      key: user.id,
      children: React.createElement(
        "button",
        { className: "float-end" },
        "Like"
      ),
    });
  })
);

```