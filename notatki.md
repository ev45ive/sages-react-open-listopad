# GIT

cd ..
git clone https://bitbucket.org/ev45ive/sages-react-open-listopad.git sages-react-open-listopad
cd sages-react-open-listopad
npm i
npm start

<!--  -->

git pull --autostash

<!--  -->

git stash -u
git pull
git pull -f

<!--  -->

git pull -u origin master

# instalacje

node -v
v14.17.0

npm -v
6.14.6

git --version
git version 2.31.1.windows.1

code -v
1.62.3
ccbaa2d27e38e5afa3e5c21c1c7bef4657064247
x64

Google Chrome 96.0.4664.45

# Extensions

https://marketplace.visualstudio.com/items?itemName=dsznajder.es7-react-js-snippets

https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode

https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi

https://marketplace.visualstudio.com/items?itemName=quicktype.quicktype

# Intelij / WebStorm

https://plugins.jetbrains.com/plugin/10113-react-snippets

https://plugins.jetbrains.com/plugin/16796-live-templates-for-typescript-and-react

## Env variables

https://www.npmjs.com/package/cross-env

## Powershell

Set-ExecutionPolicy -ExecutionPolicy Unrestricted

Select Default Profile -> GitBash

## Create React APp

npm i -g create-react-app
npm i -g create-react-app@4.0.3
npm i -g create-react-app@4.^

https://semver.org/lang/pl/
https://semver.npmjs.com/

C:\Users\PC\AppData\Roaming\npm\create-react-app -> C:\Users\PC\AppData\Roaming\npm\node_modules\create-react-app\index.js

create-react-app --version
4.0.3
create-react-app.cmd --version # Powershell

https://create-react-app.dev/

( + Configure without Ejecting -> https://github.com/gsoft-inc/craco )

npx create-react-app --help

npx create-react-app "." --use-npm --template typescript

## react-scripts

npm start

## Node 17

Error: error:0308010C:digital envelope routines::unsupported
at new Hash (node:internal/crypto/hash:67:19)

> > > SET ENV VAR >>>

```
export NODE_OPTIONS=--openssl-legacy-provider
npm start
```

lub w package.json > scripts:
"start": "export NODE_OPTIONS=--openssl-legacy-provider react-scripts start",

## UI Toolkit

https://mui.com/
https://react-bootstrap.github.io/components/tabs/
https://ant.design/docs/react/introduce

https://headlessui.dev/

https://blueprintjs.com/
https://github.com/Hipo/react-ui-toolkit
https://reakit.io/
https://www.primefaces.org/primereact/
https://www.ag-grid.com/react-data-grid/

## Playlists

mkdir -p src/core/components
mkdir -p src/core/model
mkdir -p src/playlists/containers
mkdir -p src/playlists/components

touch src/playlists/containers/PlaylistsView.tsx
touch src/playlists/components/PlaylistList.tsx
touch src/playlists/components/PlaylistDetails.tsx
touch src/playlists/components/PlaylistEditor.tsx

## Music Search

mkdir -p src/music/containers
mkdir -p src/music/components

touch src/core/model/Search.ts
touch src/music/containers/MusicSearchView.tsx
touch src/music/components/SearchForm.tsx
touch src/music/components/SearchResultsGrid.tsx
touch src/music/components/AlbumCard.tsx

## Data fetching

npm i axios

https://use-http.com/#/?id=basic-usage-auto-managed-state
https://swr.vercel.app/

## Auth2

https://github.com/Zemnmez/react-oauth2-hook

## Router

https://reactrouter.com/docs/en/v6/getting-started/overview#nested-routes

# Redux

https://redux.js.org/tutorials/fundamentals/part-5-ui-react

https://redux-toolkit.js.org/
https://redux.js.org/
https://react-redux.js.org/

https://github.com/reduxjs/reselect

https://formik.org/

you dont need redux - https://www.youtube.com/watch?v=omnwu_etHTY&ab_channel=AngularConnect

## Alternatives

https://react-query.tanstack.com/guides/dependent-queries

## Deployment

https://create-react-app.dev/docs/deployment

npm run build

## environment

https://create-react-app.dev/docs/adding-custom-environment-variables
https://create-react-app.dev/docs/using-global-variables

https://pl.reactjs.org/docs/code-splitting.html
https://stackblitz.com/github/remix-run/react-router/tree/main/examples/lazy-loading?file=src/App.tsx

```tsx
const About = React.lazy(() => import("./About"));

<Route
  path="about"
  element={
    <React.Suspense fallback={<>loading...</>}>
      <About />
    </React.Suspense>
  }
/>;

```
