import React from "react";
import { render, screen } from "@testing-library/react";
import App from "./App";
import { Provider } from "react-redux";
import { createStore } from "redux";
import { MemoryRouter, Routes, Route } from "react-router-dom";
import { Store } from "@reduxjs/toolkit";

const store = {
  getState: jest.fn(),
  dispatch: jest.fn(),
  subscribe: jest.fn(),
} as unknown as Store;

const Wrapper = (props: { children?: any }) => {
  return (
    <Provider store={store}>
      <MemoryRouter initialEntries={["/playlists"]}>
        {props.children}
      </MemoryRouter>
    </Provider>
  );
};

test("renders learn react link", async () => {
  (store.getState as jest.Mock).mockReturnValue({
    playlists: { entities: { 123: { name: "Playlist 123",  } }, list: ["123"] },
  });
  render(<App />, { wrapper: Wrapper });
  expect(store.getState).toHaveBeenCalled();

  const linkElement = await screen.findByText(/Playlist 123/i);
  expect(linkElement).toBeInTheDocument();
});
