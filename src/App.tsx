import { Route, Routes } from "react-router-dom";
import { NavBar } from "./core/components/NavBar";
import { PlaylistStateProvider } from "./core/context/PlaylistContext";
import { AlbumDetailView } from "./music/containers/AlbumDetailView";
import { ReduxSearchView } from "./music/containers/ReduxSearchView";
import { PlaylistsReducerView } from "./playlists/containers/PlaylistsReducerView";
import { SelectedPlaylistDetails } from "./playlists/containers/SelectedPlaylistDetails";

function App() {
  return (
    <>
      <NavBar />
      <div className="container">
        <div className="row">
          <div className="col">
            <PlaylistStateProvider>
              <Routes>
                <Route path="/" element={<ReduxSearchView />} />
                <Route path="/albums/:albumId" element={<AlbumDetailView />} />
                <Route path="/playlists" element={<PlaylistsReducerView />}>
                  <Route
                    path=":selectedId"
                    element={<SelectedPlaylistDetails />}
                  />
                </Route>
              </Routes>
            </PlaylistStateProvider>
          </div>
        </div>
      </div>
    </>
  );
}

export default App;
