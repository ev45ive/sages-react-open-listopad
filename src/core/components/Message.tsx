import React from "react";
import { Props } from "../hooks/useMessage";


export const Message = ({ text, type = "danger" }: Props) => text ? <div className={`alert alert-${type}`}>{text}</div> : null;
