import React, { useReducer } from "react";
import * as fromPlaylists from "../../reducers/playlists.reducer";
import { ActionType, PlaylistsState } from "../../reducers/playlists.reducer";

export const PlaylistContext = React.createContext<{
  state: PlaylistsState;
  dispatch: React.Dispatch<ActionType>;
}>({} as any);


export const PlaylistStateProvider = (props: { children: React.ReactNode }) => {
  const [state, dispatch] = useReducer(
    fromPlaylists.reducer,
    fromPlaylists.initialState
  );

  return (
    <PlaylistContext.Provider
      value={{
        state,
        dispatch,
      }}
    >
      {props.children}
    </PlaylistContext.Provider>
  );
};
