import { useEffect, useState } from "react";
import { useMessage } from "./useMessage";


export function useFetch<T, P>(params: P, fetcher: (params: P) => Promise<T>) {
  const [results, setResults] = useState<T | undefined>(undefined);
  const [message, setMessage] = useMessage();
  useEffect(() => {
    setResults(undefined);
    setMessage("");
    fetcher(params)
      .then((results) => setResults(results))
      .catch((error) => setMessage(error.message));
  }, [params]);

  return { results, message } as const;
}
