import { useEffect, useState } from "react";

export function useMessage(initialMessage = "") {
  // : [string, React.Dispatch<React.SetStateAction<string>>] {
  const [message, setMessage] = useState(initialMessage);
  useEffect(() => {
    if (message === "") return;

    const handle = setTimeout(() => {
      setMessage("");
    }, 2000);

    return () => clearTimeout(handle);
  }, [message, setMessage]);

  return [message, setMessage] as const;
}

export interface Props {
  text: string;
  type?: "danger" | "info";
}


