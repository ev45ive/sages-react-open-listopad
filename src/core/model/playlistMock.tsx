import { Playlist } from "../../core/model/Playlist";

export const playlistMock: Playlist[] = [
  {
    id: "123",
    name: "Playlist 123",
    public: true,
    description: "BEst playlist",
  },
  {
    id: "234",
    name: "Playlist 234",
    public: false,
    description: "Awesome playlist",
  },
  {
    id: "345",
    name: "Playlist 345",
    public: true,
    description: "Cool Playlist",
  },
];
