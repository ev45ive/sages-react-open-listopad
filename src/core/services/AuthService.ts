

// holoyis165@bulkbye.com
// placki777


export class AuthService implements IProvideAuthToken {
    token = '';

    constructor(
        private client_id: string,
        private redirect_uri: string,
    ) {
        // this.client_id = client_id
    }

    init() {
        if (window.location.hash) {
            // const expires_in = new URLSearchParams(window.location.hash).get('expires_in')

            const access_token = new URLSearchParams(window.location.hash).get('#access_token')
            if (access_token) {
                this.token = access_token
                window.sessionStorage.setItem('token', access_token)
                // window.sessionStorage.setItem('token_expires', expires_in)
                window.location.hash = ''
            }
        }

        this.token = window.sessionStorage.getItem('token') || '' 

        if (!this.token) {
            this.login()
        }
    }

    login() {
        var state = '123456789';

        localStorage.setItem('nonce', state);
        var scope = 'user-read-private user-read-email';

        var url = 'https://accounts.spotify.com/authorize';
        url += '?response_type=token';
        url += '&client_id=' + encodeURIComponent(this.client_id);
        url += '&scope=' + encodeURIComponent(scope);
        url += '&redirect_uri=' + encodeURIComponent(this.redirect_uri);
        url += '&state=' + encodeURIComponent(state);

        window.location.href = (url);
    }

    logout(){
        window.sessionStorage.removeItem('token')
        this.token = ''
    }

    getToken() {
        return this.token;
    }
}

export interface IProvideAuthToken {
    getToken(): string;
}
