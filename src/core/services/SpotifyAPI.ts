import axios from "axios";
import { Album, AlbumSearchResponse } from "../model/Search";
import { AuthService, IProvideAuthToken } from "./AuthService";

export class SpotifyAPI {

    constructor(private auth: IProvideAuthToken) { }

    searchAlbums = (query: string): Promise<Album[]> => {

        return axios.get<AlbumSearchResponse>( //
            `https://api.spotify.com/v1/search`, {
            params: {
                query,
                type: 'album',
                include_external: 'audio'
            },
        }).then(res => res.data.albums.items)
    }

    fetchAlbumById = (id: string): Promise<Album> => {
        return axios.get<Album>(`https://api.spotify.com/v1/albums/${id}`, {}).then(res => res.data)
    }


}
