import axios from "axios";
import { auth } from ".";

interface SpotifyError {
    error: {
        message: string;
        status: number;
    };
}
export function isSpotifyError(data: any): data is SpotifyError {
    return 'error' in data && 'message' in data.error;
}