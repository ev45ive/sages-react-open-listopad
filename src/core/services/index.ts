import axios from "axios"
import { AuthService } from "./AuthService"
import { SpotifyAPI } from "./SpotifyAPI"
import { isSpotifyError } from "./SpotifyError"

export const auth = new AuthService(
    '5b1885ea4aab4e4a92095e95741de505',
    'http://localhost:3000/'
)
export const searchAPI = new SpotifyAPI(auth)

axios.interceptors.request.use(config => {
    config.headers = {
        ...config.headers,
        ['Authorization']: `Bearer ${auth.getToken()}`
    }
    return config
})


axios.interceptors.response.use(config => config, error => {
    // Custom Type Guard Function
    if (!axios.isAxiosError(error) || !isSpotifyError(error.response?.data))
        throw 'Unexpected error';

    if (error.response?.status === 401) {
        auth.login();
    }
    return Promise.reject(new Error(error.response?.data.error.message));
});
