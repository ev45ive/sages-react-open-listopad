import React from "react";
import { Link } from "react-router-dom";
import { AlbumCardView } from "../../core/model/Search";

interface Props {
  album: AlbumCardView;
}

export const AlbumCard = ({album}: Props) => {
  return (
    <Link className="card" to={`/albums/${album.id}`}>
      <img src={album.images[0].url} className="card-img-top" alt="..." />
      <div className="card-body">
        <h5 className="card-title">{album.name}</h5>
        {/* <p className="card-text">
                This is a longer card with supporting text below as a natural
                lead-in to additional content. This content is a little bit
                longer.
              </p> */}
      </div>
    </Link>
  );
};
