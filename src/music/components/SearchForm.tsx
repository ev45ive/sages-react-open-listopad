import React, { useCallback, useEffect, useState } from "react";

interface Props {
  query: string;
  onSearch(query: string): void;
}

export function useField<T>(initialState: T) {
  const [value, setValue] = useState(initialState);

  const onChange = useCallback((e) => setValue(e.target.value), []);

  // return [value, onChange] as const;
  return { field: { value, onChange }, setValue } as const;
}

export const SearchForm = ({ onSearch, query: parentQuery }: Props) => {
  const queryField = useField(parentQuery);

  useEffect(() => queryField.setValue(parentQuery), [parentQuery]);

  return (
    <div className="mb-3">
      <div className="input-group">
        <input
          type="text"
          className="form-control"
          placeholder="Search"
          {...queryField.field}
        />
        <button
          className="btn btn-outline-secondary"
          type="button"
          onClick={() => onSearch(queryField.field.value)}
        >
          Search
        </button>
      </div>
    </div>
  );
};
