import React from "react";
import { AlbumCardView } from "../../core/model/Search";
import { AlbumCard } from "./AlbumCard";

interface Props {
  results: AlbumCardView[];
}

export const SearchResultsGrid = ({ results }: Props) => {
  return (
    <div>
      <div className="row row-cols-1 row-cols-sm-4 g-0">
        {results.map((result) => (
          <div className="col" key={result.id}>
            <AlbumCard album={result} />
          </div>
        ))}
      </div>
    </div>
  );
};

/* 
=== TODO - DO ZROBIENIA::

- Dodaj <MusicSearchView> do aplikacji zamiast <PlaylistView/>
- Przekaz albumMocks do Search Results Grid
- Przekaz result z grida do <AlbumCard>
- Wyswietl wartosc query ('batman')  w formularzu
- Klikniecie 'search powinno zmienic wartosc query w rodzicu
- Efekt wyświetla query w console.log gdy sie zmieni

*/
