import React, { useRef } from "react";
import { useParams } from "react-router";
import { Loading } from "../../core/components/Loading";
import { Message } from "../../core/components/Message";
import { useFetch } from "../../core/hooks/useFetch";
import { Track } from "../../core/model/Search";
import { searchAPI } from "../../core/services";
import { AlbumCard } from "../components/AlbumCard";

/* === TODO: 
- open http://localhost:3000/albums/5Tby0U5VndHW0SomYO7Id7
- show albumId param on screen (useParams)
- use SpotifyAPI Service - fetchAlbumById ( param goes here)
- show loader
- show name, id, artist, album card when LOaded!!!
+ show error messsage
+ Link from Search REsults to here!
*/

export const AlbumDetailView = () => {
  const { albumId } = useParams();

  const { message, results: album } = useFetch(
    albumId || "",
    searchAPI.fetchAlbumById
  );

  const audioRef = useRef<HTMLAudioElement>(null);

  const play = (track: Track) => {
    if (!audioRef.current) return;
    audioRef.current.src = track.preview_url;
    audioRef.current.volume = 0.1;
    audioRef.current.play();
  };

  if (!album) return message ? <Message text={message} /> : <Loading />;

  return (
    <div>
      <div className="row">
        <div className="col">
          <h1 className="display-4">{album.name}</h1>
          <small className="text-muted"> {albumId} </small>
          <hr />
        </div>
      </div>
      <div className="row">
        <div className="col">
          <AlbumCard album={album} />
        </div>
        <div className="col">
          <dl>
            <dt>Name:</dt>
            <dd>{album.name}</dd>
            <dt>Artist:</dt>
            <dd>{album.artists[0].name} </dd>
          </dl>

        {/* https://developer.spotify.com/documentation/web-playback-sdk/quick-start/ */}
          <audio ref={audioRef} controls className="w-100"></audio>

          <div className="list-group">
            {album.tracks.items.map((track) => (
              <div className="list-group-item" onClick={() => play(track)}>
                {track.name}
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};
