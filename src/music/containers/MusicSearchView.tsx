import React, { useEffect, useState } from "react";
import { Message } from "../../core/components/Message";
import { searchAPI } from "../../core/services";
import { SearchForm } from "../components/SearchForm";
import { SearchResultsGrid } from "../components/SearchResultsGrid";
import { Loading } from "../../core/components/Loading";
import { useFetch } from "../../core/hooks/useFetch";
import { useNavigate } from "react-router";
import { useSearchParams } from "react-router-dom";

interface Props {}

export const MusicSearchView = (props: Props) => {
  const navigate = useNavigate();
  const [searchParams, setSearchParams] = useSearchParams({ q: "" });
  const [query, setQuery] = useState("");

  const { results, message } = useFetch(query, searchAPI.searchAlbums);

  useEffect(
    () => setQuery(searchParams.get("q") || ""),
    [searchParams.get("q")]
  );

  return (
    <div>
      <div className="row">
        <div className="col">
          <h3 className="display-3">Search Albums</h3>
          <SearchForm
            query={query}
            onSearch={(query) => {
              setSearchParams({ q: query });
              // setQuery(query);
            }}
          />
        </div>
      </div>
      <div className="row">
        <div className="col">
          {!message && !results && <Loading />}
          {message && <Message text={message} />}

          {results && <SearchResultsGrid results={results} />}
        </div>
      </div>
    </div>
  );
};
