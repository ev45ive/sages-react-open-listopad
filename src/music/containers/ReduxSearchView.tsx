import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useSearchParams } from "react-router-dom";
import { Loading } from "../../core/components/Loading";
import { Message } from "../../core/components/Message";
import { searchAPI } from "../../core/services";
import { asyncSearchAlbums, selectors } from "../../reducers/search.reducer";
import { useAppSelector } from "../../store";
import { SearchForm } from "../components/SearchForm";
import { SearchResultsGrid } from "../components/SearchResultsGrid";

interface Props {}

export const ReduxSearchView = (props: Props) => {
  const [searchParams, setSearchParams] = useSearchParams({ q: "" });
  const query = searchParams.get("q") || "batman";
  
  const dispatch = useDispatch();
  const results = useSelector(selectors.selectAllResults);
  const message = useAppSelector((state) => state.search.message);
  const loading = useAppSelector((state) => state.search.loading);

  useEffect(() => {
    dispatch(asyncSearchAlbums(query));
  }, [query]);

  return (
    <div>
      <div className="row">
        <div className="col">
          <h3 className="display-3">Search Albums</h3>
          <SearchForm
            query={searchParams.get("q")!}
            onSearch={(query) => setSearchParams({ q: query })}
          />
        </div>
      </div>
      <div className="row">
        <div className="col">
          {loading && <Loading />}
          {message && <Message text={message} />}

          {results && <SearchResultsGrid results={results} />}
        </div>
      </div>
    </div>
  );
};
