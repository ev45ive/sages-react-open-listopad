import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import React from "react";
import renderer from "react-test-renderer";
import { PlaylistDetails } from ".";
import { playlistMock } from "../../core/model/playlistMock";

test("renders empty details", () => {
  render(<PlaylistDetails onEdit={() => {}} />);

  screen.getByText("Please select playlist");
});

test("renders example playlist details", () => {
  const playlist = playlistMock[0];
  render(<PlaylistDetails playlist={playlist} onEdit={() => {}} />);
  expect(screen.queryByText("Please select playlist")).not.toBeInTheDocument();

  expect(screen.getByTestId("playlist_name")).toHaveTextContent(playlist.name);

  expect(screen.getByLabelText(/Public/)).toHaveTextContent("Yes");
});

test("renders example playlist details - shapshot", () => {
  const playlist = playlistMock[0];
  const tree = renderer
    .create(<PlaylistDetails playlist={playlist} onEdit={() => {}} />)
    .toJSON();

  expect(tree).toMatchSnapshot();
});

test("clicking edit emits event", () => {
  const playlist = playlistMock[0];
  const editSpy = jest.fn();

  render(<PlaylistDetails playlist={playlist} onEdit={editSpy} />);

  const btn = screen.getByRole("button", { name: "Edit" });
  userEvent.click(btn);

  expect(editSpy).toHaveBeenCalledWith(playlist.id);
});
