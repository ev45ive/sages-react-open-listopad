import React from "react";
import { Playlist } from "../../core/model/Playlist";
import styles from "./playlists.module.css";

interface Props {
  // playlist: Playlist |undefined
  playlist?: Playlist;
  onEdit: (id: Playlist["id"]) => void;
}

export const PlaylistDetails = React.memo(
  ({ playlist, onEdit }: Props) => {
    if (!playlist) {
      return <p className="alert alert-info">Please select playlist</p>;
    }
    // console.log('render Details')

    return (
      <div>
        <dl>
          <dt>Name:</dt>
          <dd data-testid="playlist_name">{playlist.name}</dd>

          <dt id="playlist_public">Public:</dt>
          <dd
            aria-labelledby="playlist_public"
            className={playlist.public ? styles.isPublic : styles.isPrivate}
          >
            {playlist.public ? "Yes" : "No"}
          </dd>

          <dt>Description:</dt>
          <dd>{playlist.description}</dd>
        </dl>
        <button className="btn btn-info" onClick={() => onEdit(playlist.id)}>
          Edit
        </button>
      </div>
    );
  }
  // ,
  // /* propsAreEqual */ (
  //   prevProps: Props,
  //   nextProps: Props
  // ) => prevProps.playlist === nextProps.playlist
);
