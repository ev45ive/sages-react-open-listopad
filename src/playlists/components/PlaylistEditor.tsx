import React, {
  forwardRef,
  useEffect,
  useImperativeHandle,
  useRef,
  useState,
} from "react";
import { Playlist } from "../../core/model/Playlist";

interface Props {
  playlist?: Playlist;
  onCancel: () => void;
  onSave: (draft: Playlist) => void;
  // onDirty
}

const emptyPlaylist = {
  id: "",
  name: "",
  description: "",
  public: false,
};

export const PlaylistEditor = forwardRef<{ isDirty: boolean }, Props>(
  ({ playlist = emptyPlaylist, onCancel, onSave }: Props, ref) => {
    const [playlistName, setPlaylistName] = useState(playlist.name);
    const [playlistPublic, setPlaylistPublic] = useState(playlist.public);
    const [playlistDescription, setPlaylistDescription] = useState(
      playlist.description
    );
    const [isDirty, setIsDirty] = useState(false);

    useImperativeHandle(
      ref,
      () => {
        return {
          isDirty,
        };
      },
      [isDirty]
    );

    useEffect(() => {
      setPlaylistName(playlist.name);
      setPlaylistPublic(playlist.public);
      setPlaylistDescription(playlist.description);
    }, [playlist]);

    const inputRef = useRef<HTMLInputElement>(null);
    useEffect(() => {
      // document.getElementById("playlist_name")?.focus();
      inputRef.current?.focus();
    }, []);

    const nameChangeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
      setIsDirty(e.target.value !== playlist.name);
      setPlaylistName(e.target.value);
    };

    const submit = () => {
      const draft = {
        ...playlist,
        name: playlistName,
        public: playlistPublic,
        description: playlistDescription,
      };
      onSave(draft);
    };
    return (
      <div>
        {/* <pre>{JSON.stringify(playlist, null, 2)}</pre>
        <pre>
          {JSON.stringify(
            {
              playlistName,
              playlistPublic,
              playlistDescription,
            },
            null,
            2
          )}
        </pre> */}
        <div className="form-group mb-3">
          <label htmlFor="playlist_name">Name:</label>
          <input
            type="text"
            className="form-control"
            name="playlist_name"
            id="playlist_name"
            ref={inputRef}
            placeholder="Name"
            value={playlistName}
            onChange={nameChangeHandler}
          />
          <small id="counter" className="form-text text-muted float-end">
            {playlist.name.length} / 170
          </small>
        </div>

        <div className="form-check mb-3">
          <label className="form-check-label">
            <input
              type="checkbox"
              className="form-check-input"
              checked={playlistPublic}
              onChange={(e) => setPlaylistPublic(e.target.checked)}
            />
            Public
          </label>
        </div>

        <div className="form-group mb-3">
          <label htmlFor="playlist_description">Description</label>
          <textarea
            className="form-control"
            name="playlist_description"
            id="playlist_description"
            rows={3}
            value={playlistDescription}
            onChange={(e) => setPlaylistDescription(e.target.value)}
          ></textarea>
        </div>

        <button className="btn btn-danger" onClick={onCancel}>
          Cancel
        </button>
        <button className="btn btn-success" onClick={submit}>
          Save
        </button>
      </div>
    );
  }
);

// PlaylistEditor.defaultProps = {
//   playlist: {
//     id: "",
//     name: "",
//     description: "",
//     public: false,
//   },
// };
