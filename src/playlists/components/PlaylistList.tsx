import React from "react";
import { Playlist } from "../../core/model/Playlist";
import { cls } from "./utils";

interface Props {
  playlists: Playlist[];
  selectedId?: string;
  onSelect: (id: string) => void;
  onDelete: (id: string) => void;
}

export const PlaylistList = ({
  playlists,
  selectedId,
  onSelect,
  onDelete,
}: Props) => {
  return (
    <div>
      <div className="list-group">
        {playlists.map((playlist, index) => {
          return (
            <div
              className={cls(
                "list-group-item",
                selectedId === playlist.id && "active"
              )}
              key={playlist.id}
              onClick={() => onSelect(playlist.id)}
            >
              {index + 1}. {playlist.name}
              <span
                className="close float-end"
                onClick={(event) => {
                  event.stopPropagation() // dont bubble to parents
                  onDelete(playlist.id)
                }}
              >
                &times;
              </span>
            </div>
          );
        })}
      </div>
    </div>
  );
};
