
const classes: {
    isPublic: string,
    /**
     * Playlist private color
     */
    isPrivate: string,
};
export default classes;