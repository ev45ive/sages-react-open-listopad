// https://www.npmjs.com/package/classnames
// npm install classnames
export const cls = (...classOrCond: (string | boolean)[]): string => {
  return classOrCond.filter((c) => c !== "").join(" ");
};
