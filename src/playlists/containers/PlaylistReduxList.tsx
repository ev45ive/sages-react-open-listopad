import React from "react";
import { NavLink } from "react-router-dom";
import * as fromPlaylists from "../../reducers/playlists-redux.reducer";
import { useAppSelector } from "../../store";


export const PlaylistReduxList = () => {
  const playlists = useAppSelector(fromPlaylists.selectors.selectAllPlaylists);

  return (
    <div className="list-group">
      {playlists.map((p) => (
        <NavLink
          className="list-group-item"
          to={"/playlists/" + p.id}
          key={p.id}
        >
          {p.name}
        </NavLink>
      ))}
    </div>
  );
};
