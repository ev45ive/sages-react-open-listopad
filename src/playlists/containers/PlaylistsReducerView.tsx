import React, { useCallback, useContext, useEffect, useReducer } from "react";
import { useStore } from "react-redux";
import { Outlet, useNavigate } from "react-router-dom";
import { PlaylistContext } from "../../core/context/PlaylistContext";
import { playlistMock } from "../../core/model/playlistMock";
import * as fromPlaylists from "../../reducers/playlists-redux.reducer";
import { useAppDispatch } from "../../store";
import { PlaylistReduxList } from "./PlaylistReduxList";

export const PlaylistsReducerView = () => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(fromPlaylists.loadPlaylists(playlistMock));
  }, []);

  return (
    <div>
      <div className="row">
        <div className="col">
          <PlaylistReduxList />
        </div>
        <div className="col">
          <Outlet />
        </div>
      </div>
    </div>
  );
};
