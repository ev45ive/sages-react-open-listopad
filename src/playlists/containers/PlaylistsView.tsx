import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from "react";
import { PlaylistDetails, PlaylistEditor, PlaylistList } from "../components";
import { Playlist } from "../../core/model/Playlist";
import { useMessage } from "../../core/hooks/useMessage";
import { Message } from "../../core/components/Message";
import { playlistMock } from "../../core/model/playlistMock";

interface Props {}


export const PlaylistsView = ({}: Props) => {
  const [mode, setMode] = useState<"details" | "edit" | "create">("details");
  const [playlists, setPlaylists] = useState(playlistMock);
  const [selectedId, setSelectedId] = useState<Playlist["id"] | undefined>();
  const [selected, setSelected] = useState<Playlist | undefined>();
  const [filter, setFilter] = useState("");
  const editorRef = useRef<{ isDirty: boolean }>(null);

  // Custom Hook
  const [message, setMessage] = useMessage();

  useEffect(() => {
    setSelected(playlists.find((p) => p.id === selectedId));
  }, [selectedId, playlists]);

  const showEditor = useCallback(() => setMode("edit"), []);

  const showDetails = useCallback(() => setMode("details"), []);

  const deletePlaylist = useCallback((id: string) => {
    setPlaylists((playlists) => playlists.filter((p) => p.id !== id));
  }, []);

  const selectPlaylist = useCallback((id: string) => {
    if (editorRef.current?.isDirty) {
      return setMessage("You have unsaved changes" + Date.now());
    }
    setSelectedId(id);
  }, []);

  const updatePlaylist = useCallback((draft: Playlist) => {
    setPlaylists((playlists) =>
      playlists.map((p) => (p.id === draft.id ? draft : p))
    );
    setMode("details");
  }, []);

  const createPlaylist = useCallback((draft: Playlist) => {
    draft.id = Date.now().toString() + ~~(Math.random() * 100_000);
    setPlaylists((playlists) => [...playlists, draft]);
    setSelectedId(draft.id);
    setMode("details");
  }, []);

  const emptyPlaylist = useMemo(
    () => ({
      id: "",
      name: "",
      description: "",
      public: false,
    }),
    []
  );

  return (
    <div>
      <h3 className="display-3">Playlists</h3>

      <Message text={message} />

      <div className="row">
        <div className="col">
          <div className="form-group mb-3">
            <input
              type="text"
              className="form-control"
              placeholder="Filter"
              value={filter}
              onChange={(e) => setFilter(e.target.value)}
            />
          </div>

          <PlaylistList
            onSelect={selectPlaylist}
            onDelete={deletePlaylist}
            playlists={playlists}
            selectedId={selectedId}
          />
          <button
            className="mt-3 btn btn-info float-end"
            onClick={() => setMode("create")}
          >
            Create new playlist
          </button>
        </div>
        <div className="col">
          {mode === "details" && (
            <PlaylistDetails playlist={selected} onEdit={showEditor} />
          )}

          {mode === "edit" && selected && (
            <PlaylistEditor
              ref={editorRef}
              playlist={selected}
              onCancel={showDetails}
              onSave={updatePlaylist}
            />
          )}

          {mode === "create" && (
            <PlaylistEditor
              playlist={emptyPlaylist}
              onCancel={showDetails}
              onSave={createPlaylist}
            />
          )}
        </div>
      </div>
    </div>
  );
};
