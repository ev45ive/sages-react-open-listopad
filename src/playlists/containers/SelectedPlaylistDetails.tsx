import React, { useContext, useEffect } from "react";
import { PlaylistContext } from "../../core/context/PlaylistContext";
import { PlaylistDetails } from "../components";
import * as fromPlaylists from "../../reducers/playlists-redux.reducer";
import { useNavigate, useParams } from "react-router";
import { useAppSelector } from "../../store";

export const SelectedPlaylistDetails = () => {
  const { selectedId } = useParams();
  const navigate = useNavigate();

  const selected = useAppSelector(
    fromPlaylists.selectors.selectPlaylistById(selectedId)
  );

  useEffect(() => {}, [selectedId]);

  return (
    <PlaylistDetails
      playlist={selected}
      onEdit={() => navigate(`/playlists/${selectedId}/edit`)}
    />
  );
};
