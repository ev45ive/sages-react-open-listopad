import { AnyAction } from "@reduxjs/toolkit";

// { type: 'INC', payload: number } | { type: 'DEC', payload: number }
export const counterReducer = (state = 0, action: AnyAction) => {
    switch (action.type) {
        case 'INC': return state + action.payload;
        case 'DEC': return state - action.payload;
        default: return state
    }
}