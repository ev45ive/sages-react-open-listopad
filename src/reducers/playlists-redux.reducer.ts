import { createSelector, createSlice, PayloadAction } from '@reduxjs/toolkit'
import { Playlist } from '../core/model/Playlist'
import { RootState } from '../store'

export interface PlaylistsState {
    entities: {
        [playlist_id: Playlist['id']]: Playlist
    }
    list: Playlist['id'][]
}

const initialState: PlaylistsState = {
    entities: {},
    list: []
}

export const playlistsSlice = createSlice({
    name: 'playlists',
    initialState,
    reducers: {
        loadPlaylists: (state, action: PayloadAction<Playlist[]>) => {
            // Immer.js
            state.list = []
            action.payload.forEach(p => {
                state.entities[p.id] = p
                state.list.push(p.id)
            })
        }
    }
})

export const { loadPlaylists } = playlistsSlice.actions


const feature = (state: RootState) => state[playlistsSlice.name];

const entities = createSelector(feature, state => state.entities)
const list = createSelector(feature, state => state.list)

const selectAllPlaylists = createSelector(
    entities,
    list,
    (entities, list) => list.map(id => entities[id])
)
const selectPlaylistById = (id?: Playlist['id']) => createSelector(
    entities,
    (entities) => id !== undefined && entities[id] || undefined
)

export const selectors = { selectAllPlaylists, selectPlaylistById }

export default playlistsSlice.reducer