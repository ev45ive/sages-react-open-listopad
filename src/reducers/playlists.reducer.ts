import { Playlist } from "../core/model/Playlist";

export interface PlaylistsState {
    // entities: { [id: Playlist['id']]: Playlist }
    // list: Playlist['id'][]

    items: Playlist[];
    selectedId?: Playlist["id"];
    loading: boolean;
    message: string;
}

export const initialState: PlaylistsState = {
    items: [],
    loading: false,
    message: "",
};

export const reducer = (state = initialState, action: ActionType): PlaylistsState => {
    switch (action.type) {
        case "[Playlists] Load Start":
            return { ...state, loading: true };

        case "[Playlists] Load Success":
            return { ...state, loading: false, items: action.payload };

        case "[Playlists] Load Failed":
            return { ...state, loading: false, message: action.error.message };

        case "[Playlists] Selected":
            return { ...state, selectedId: action.payload.id };

        case "[Playlists] Created":
            return {
                ...state, selectedId: action.payload.draft.id,
                items: [...state.items, action.payload.draft]
            };

        case "[Playlists] Updated": {
            const draft = action.payload.draft;
            return {
                ...state, selectedId: draft.id,
                items: state.items.map(p => p.id === draft.id ? draft : p)
            };
        }
        case "[Playlists] Deleted":
            return {
                ...state,
                items: state.items.filter(p => p.id !== action.payload.id),
                selectedId: state.selectedId === action.payload.id ? undefined : state.selectedId
            };

        default:
            const _never: never = action;
            return state;
    }
};

export type ActionType = ReturnType<
    | typeof PlaylistsLoadStart
    | typeof PlaylistsLoadSuccess
    | typeof PlaylistsLoadFailed
    | typeof PlaylistsSelected
    | typeof PlaylistsDelete
    | typeof PlaylistsUpdate
    | typeof PlaylistsCreate
>;

export const PlaylistsLoadStart = () => ({
    type: "[Playlists] Load Start" as const,
});
export const PlaylistsLoadSuccess = (payload: Playlist[]) => ({
    type: "[Playlists] Load Success" as const,
    payload,
});
export const PlaylistsLoadFailed = (error: Error) => ({
    type: "[Playlists] Load Failed" as const,
    error,
});
export const PlaylistsSelected = (id?: Playlist["id"]) => ({
    type: "[Playlists] Selected" as const,
    payload: { id },
});
export const PlaylistsDelete = (id: Playlist["id"]) => ({
    type: "[Playlists] Deleted" as const,
    payload: { id },
});
export const PlaylistsUpdate = (draft: Playlist) => ({
    type: "[Playlists] Updated" as const,
    payload: { draft },
});
export const PlaylistsCreate = (draft: Playlist) => ({
    type: "[Playlists] Created" as const,
    payload: { draft },
});

/* TODO: 
    - Create Actions, types and update reducer for:
        - Add new Playlist
        - Update playlist
        - Delete playlist
*/


/* selectors */

export const selectAllPlaylists = (state: PlaylistsState) => state.items
export const selectSelectedPlaylist = (state: PlaylistsState) => {
    return state.items.find(p => p.id === state.selectedId)
}