import { createAction, createAsyncThunk, createSelector, createSlice, PayloadAction } from '@reduxjs/toolkit'
import { Dispatch } from 'react'
import { Playlist } from '../core/model/Playlist'
import { Album } from '../core/model/Search'
import { searchAPI } from '../core/services'
import { RootState } from '../store'

export interface SearchState {
    entities: {
        albums: { [id: Playlist['id']]: Album }
    }
    results: string[]
    loading: boolean
    message: string
}

const initialState: SearchState = {
    entities: {
        albums: {}
    },
    results: [],
    loading: false,
    message: '',
}

const sharedActionExample = createAction<number>('shared')
// sharedActionExample(123)
// sharedActionExample.type


export const asyncSearchAlbums = createAsyncThunk(
    'search/albums',
    // Declare the type your function argument here:
    async (query: string, thunkApi) => {
        // thunkApi.dispatch(...)
        return searchAPI.searchAlbums(query)
    }
)

export const searchSlice = createSlice({
    name: 'search',
    initialState,
    reducers: {},
    extraReducers: builder => builder
        // .addCase(sharedActionExample, (state, action) => {
        //     action.payload
        // })
        .addCase(asyncSearchAlbums.fulfilled, (state, action) => {
            state.results = []
            action.payload.forEach(p => {
                state.entities.albums[p.id] = p
                state.results.push(p.id)
            })
            state.loading = false
        })
        /// =====
        .addCase(asyncSearchAlbums.pending, state => { state.loading = true })
        .addCase(asyncSearchAlbums.rejected, (state, action) => {
            state.loading = false
            state.message = action.error.message || ''
        })
})

export const { /* loadSearchResults */ } = searchSlice.actions


const feature = (state: RootState) => state[searchSlice.name];

const albums = createSelector(feature, state => state.entities.albums)
const list = createSelector(feature, state => state.results)

const selectAllResults = createSelector(
    albums,
    list,
    (entities, list) => list.map(id => entities[id])
)
const selectPlaylistById = (id?: Playlist['id']) => createSelector(
    albums,
    (entities) => id !== undefined && entities[id] || undefined
)

export const selectors = { selectAllResults, selectPlaylistById }

export default searchSlice.reducer




// export const asyncSearchAlbums = (query: string) => //
//     (dispatch: Dispatch<any>) => {
//         /* dispatch( searchStart) */
//         searchAPI.searchAlbums(query).then((results) => {
//             dispatch(loadSearchResults(results))
//         }).catch(error => {/* dispatch( searchFailed(error)) */ })
//     }