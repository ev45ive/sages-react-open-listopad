import { AnyAction, configureStore, Dispatch, Middleware, MiddlewareAPI } from '@reduxjs/toolkit'
import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';
import { counterSlice } from './reducers/counter.slice.reducer';
import { counterReducer } from './reducers/couter'
import playlists, { playlistsSlice } from './reducers/playlists-redux.reducer';
import search, { searchSlice } from './reducers/search.reducer';

// Redux Slices:
// redux.createStore( combineReducers({
//     counter: counterReducer,
//     // ui: uiReducer,
//     // search: searchReducer,
// }) )

 
// https://www.npmjs.com/package/redux-promise

// https://www.npmjs.com/package/redux-logger
const logger: Middleware = (api: MiddlewareAPI<AppDispatch, RootState>) => //
    (next: Dispatch<AnyAction>) => //
        (action: any) => {
            console.log(action);
            next(action)
        }

// const thunk: Middleware = (api: MiddlewareAPI<AppDispatch, RootState>) => //
//     (next: Dispatch<AnyAction>) => //
//         (action: any) => {
//             if (typeof action === 'function') {
//                 action(api.dispatch, api.getState())
//             } else {
//                 next(action)
//             }
//         }


export const store = configureStore({
    reducer: {
        oldcounter: counterReducer,
        [counterSlice.name]: counterSlice.reducer,
        [playlistsSlice.name]: playlists,
        [searchSlice.name]: search,
        // ui: uiReducer,
        // search: searchReducer,

    },
    // Middleware
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware()
            .prepend(logger)
});

(window as any).store = store

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch

export const useAppDispatch = () => useDispatch<AppDispatch>()
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector


